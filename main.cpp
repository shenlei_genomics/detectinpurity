#pragma once
#include <iostream>
#include <vector>
#include <io.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <map>
#include <fstream>
#include <set>

enum ERROR_CODE
{
	PATH_NOT_EXIST = -1,
	GET_IMAGES_FAILED = -2
};
using std::string;
using std::vector;

const unsigned int small_area = 5*5;
const unsigned int mudium_area = 15 * 15;
const unsigned int large_area = 1000;

static double TH = 2560.0/256;

int getFiles(const std::string& path, const std::string& ext, std::vector<std::string>& files)
{
	intptr_t file_handle = 0;
	struct _finddata_t file_info;
	std::string path_name, ext_name;
	if (strcmp(ext.c_str(), "") != 0)//if not empty
		ext_name = "\\*" + ext;
	else
		ext_name = "\\*";

	if ((file_handle = _findfirst(path_name.assign(path).append(ext_name).c_str(), &file_info)) != -1)
	{
		do
		{
			if (file_info.attrib & _A_SUBDIR)
			{
				if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
				{
					getFiles(path_name.assign(path).append("\\").append(file_info.name), ext, files);
				}
			}
			else
			{
				if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
				{
					files.push_back(path_name.assign(path).append("\\").append(file_info.name));
					//files.push_back(file_info.name);
				}
			}
		} while (_findnext(file_handle, &file_info) == 0);
		_findclose(file_handle);
	}
	return 0;
}


static void help(char* progName)
{
	std::cout << std::endl
		<< "This program output image's inpurity info:" << std::endl
		<< "Usage:" << std::endl
		<< progName << " imgs_directory result_csv_path [-t threshold]" << std::endl
		<< "For example:" << std::endl
		<< progName << " E:/Lane1/Cycle1  ./result.csv" << std::endl;	
}

/*@belief: two-passing algorithm to find connected components. codes and method are from: https://blog.csdn.net/icvpr/article/details/10259577
@_binImg : input binary image
@_lableImg: labled img
*/
void icvprCcaByTwoPass(const cv::Mat& _binImg, cv::Mat& _lableImg, std::map<int, int>& cc)
{
	// connected component analysis (4-component)  
	// use two-pass algorithm  
	// 1. first pass: label each foreground pixel with a label  
	// 2. second pass: visit each labeled pixel and merge neighbor labels  
	//   
	// foreground pixel: _binImg(x,y) = 1  
	// background pixel: _binImg(x,y) = 0  


	if (_binImg.empty() ||
		_binImg.type() != CV_8UC1)
	{
		return;
	}

	// 1. first pass  

	_lableImg.release();
	_binImg.convertTo(_lableImg, CV_32SC1);

	//std::cout << _lableImg << std::endl;

	int label = 1;  // start by 2  
	std::vector<int> labelSet;
	labelSet.push_back(0);   // background: 0  
	labelSet.push_back(1);   // foreground: 1  

	int rows = _binImg.rows - 1;
	int cols = _binImg.cols - 1;
	for (int i = 1; i < rows; i++)
	{
		int* data_preRow = _lableImg.ptr<int>(i - 1);
		int* data_curRow = _lableImg.ptr<int>(i);
		for (int j = 1; j < cols; j++)
		{
			if (data_curRow[j] == 1)
			{
				std::vector<int> neighborLabels;
				neighborLabels.reserve(2);
				int leftPixel = data_curRow[j - 1];
				int upPixel = data_preRow[j];
				if (leftPixel > 1)
				{
					neighborLabels.push_back(leftPixel);
				}
				if (upPixel > 1)
				{
					neighborLabels.push_back(upPixel);
				}

				if (neighborLabels.empty())
				{
					labelSet.push_back(++label);  // assign to a new label  
					data_curRow[j] = label;
					labelSet[label] = label;
				}
				else
				{
					std::sort(neighborLabels.begin(), neighborLabels.end());
					int smallestLabel = neighborLabels[0];
					data_curRow[j] = smallestLabel;

					// save equivalence  
					for (size_t k = 1; k < neighborLabels.size(); k++)
					{
						int tempLabel = neighborLabels[k];
						int& oldSmallestLabel = labelSet[tempLabel];
						if (oldSmallestLabel > smallestLabel)
						{
							labelSet[oldSmallestLabel] = smallestLabel;
							oldSmallestLabel = smallestLabel;
						}
						else if (oldSmallestLabel < smallestLabel)
						{
							labelSet[smallestLabel] = oldSmallestLabel;
						}
					}
				}
			}
		}
	}

	// update equivalent labels  
	// assigned with the smallest label in each equivalent label set  
	for (size_t i = 2; i < labelSet.size(); i++)
	{
		int curLabel = labelSet[i];
		int preLabel = labelSet[curLabel];
		while (preLabel != curLabel)
		{
			curLabel = preLabel;
			preLabel = labelSet[preLabel];
		}
		labelSet[i] = curLabel;
	}

	//std::cout << _lableImg << std::endl;

	// 2. second pass  
	cc.clear();
	for (int i = 0; i < rows; i++)
	{
		int* data = _lableImg.ptr<int>(i);
		for (int j = 0; j < cols; j++)
		{
			int& pixelLabel = data[j];
			pixelLabel = labelSet[pixelLabel];
			if (pixelLabel > 1)
			{
				if (cc.find(pixelLabel) != cc.end())
				{
					cc[pixelLabel] += 1;
				}
				else
				{
					cc[pixelLabel] = 1;
				}				 
			}
		}
	}
}

void InitMat(cv::Mat& m, unsigned char* num)
{
	for (int i = 0; i<m.rows; i++)
		for (int j = 0; j<m.cols; j++)
			m.at<unsigned char>(i, j) = *(num + i*m.rows + j);
}

void testTwopassing()
{
	unsigned char m0[] = { 0,0,1,0,0,1,0,
	                       1,1,1,0,1,1,1,	                                        
	                       0,0,1,0,0,1,0,
	                       0,1,1,0,1,1,0};

	cv::Mat M0(4, 7, CV_8U);
	InitMat(M0, m0);
	cv::Mat labImg;
	std::map<int, int> cc;
	icvprCcaByTwoPass(M0, labImg, cc);
}

enum size
{
	small=0, mudium, large
};
struct Inpurity
{
	unsigned int count;
	unsigned int sum_area;
	double area_pro;
	Inpurity() :count(0), sum_area(0),area_pro(0) {};
	Inpurity(const unsigned int& c, const unsigned int& sa,const double& p) :count(c), sum_area(sa), area_pro(p) {};
};


int statInpuritySize(const std::map<int, int>& cc, std::vector<Inpurity>& inps)
{
	if (cc.size() < 0)
	{
		return -1;
	}
	for each (auto c in cc)
	{
		const int area = c.second;
		if (area > small_area)
		{
			if (area < mudium_area)
			{
				++(inps[small].count);
				inps[small].sum_area += area;
				continue;
			}
			if (area < large_area)
			{
				++(inps[mudium].count);
				inps[mudium].sum_area += area;				
			}
			else
			{
				++(inps[large].count);
				inps[large].sum_area += area;
			}
		}	
	}
	return 0;
}

void writeResult(const std::string& fn, const std::vector<std::string>& files,const std::vector<std::vector<Inpurity>>& inps_vec)
{
	std::ofstream fs(fn);
	if (!fs)
	{
		std::cerr << "Write result file " << fn << " error!" << std::endl;
		return;
	}
	assert(files.size() == inps_vec.size());
	fs << "img_path,samll_inpurity_count,samll_inpurity_area_propotion(100%),medium_inpurity_count,medium_inpurity_area_propotion(100%),large_inpurity_count,large_inpurity_area_propotion(100%)\n";
	for(size_t i = 0; i < files.size(); ++i)
	{
		fs << files[i] << ",";
		for (size_t j = 0; j < 3; j++)
		{
			fs << inps_vec[i][j].count << ",";
			fs << inps_vec[i][j].area_pro << ",";
		}
		fs << "\n";
	}
	fs.close();
}

const std::string channels[] = { ".A.",".C.",".G.",".T." };
void getTotalStat(const std::vector<std::string>& files, const std::vector<std::vector<Inpurity>>& inps_vec, std::map<std::string,vector<float>> ch_area_pro)
{
	
	auto itf = files.cbegin();
	auto iti = inps_vec.cbegin();
	assert(files.size() == inps_vec.size());
	for ( ;itf != files.cend(); ++itf,++iti)
	{
		for (size_t i = 0; i < 4; i++)
		{
			if (itf->find(channels[i], 0) != string::npos)
			{
				if (ch_area_pro.find(channels[i]) != ch_area_pro.end())
				{
					for (size_t j = 0; j < 3; j++)
					{
						ch_area_pro[channels[i]][j] += (*iti)[j].area_pro;
					}
				}
				else
				{
					vector<float> pro = { 0.0f,0.0f,0.0f };
					ch_area_pro[channels[i]] = pro;
				}
				break;
			}
		}
	}	
}

void morphologyProcess(cv::Mat& bin_img)
{	
	using namespace cv;
	int erosion_type = MORPH_ELLIPSE;
	int erosion_size = 2;
	Mat element = getStructuringElement(erosion_type,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));

	/// Apply the erosion operation
	//erode(bin_img, bin_img, element);	
	imwrite("./before_median_blur.tif", bin_img);
	medianBlur(bin_img, bin_img, 5);
	imwrite("./after_median_blur.tif", bin_img);
	dilate(bin_img, bin_img, element);
}


float getImgTh(cv::Mat& img, double otsu_th)
{
	using std::vector;
	const int bins = 256;
	float hist[bins] = { 0.0f };
	int index = 0;
	for (size_t i = 0; i < img.rows; i++)
	{
		unsigned char* data = img.ptr<unsigned char>(i);
		for (size_t j = 0; j < img.cols; j++)
		{
			++(hist[data[j]]);
		}
	}
	const float kernel[5] = {0.2,0.6,0.2};
	float smoothed_hist[bins] = { 0.0f };
	size_t start = static_cast<size_t>(otsu_th);
	for (size_t i = start; i < bins; i++)
	{
		for (size_t j = 0; j < 3; j++)
		{
			smoothed_hist[i] += hist[i - 1 + j] * kernel[j];
		}
	}
	vector<float> smoothed_hist_vec(smoothed_hist, smoothed_hist + bins);
	std::vector<float>::iterator biggest_it = std::max_element(smoothed_hist_vec.begin(), smoothed_hist_vec.end());
	float half_top = *biggest_it / 2.0f;
	float min_diff = 2000.0f * 2000.0f;
	decltype(biggest_it) half_his_idx;
	for (auto it = biggest_it; it != smoothed_hist_vec.end(); ++it)
	{
		float d = abs(*it - half_top);
		if (d < min_diff)
		{
			min_diff = d;
			half_his_idx = it;
		}
	}
	//gaussian half-width = 2.355*sigma, the top to half-top distance = 1.1775*sigma, set threshold to 2.58*sigma+top_index, this will cover about 99.73% background
	auto hsigma = std::distance(biggest_it, half_his_idx);
	auto m = std::distance(smoothed_hist_vec.begin(), biggest_it);
	float th = static_cast<float>(m) + 2.58f*static_cast<float>(hsigma);
	return th;
}

int main(int argc, char* argv[])
{
	using std::string;
	using std::vector;
	using namespace cv;

	//testTwopassing();
	
	if (argc < 3)
	{
		help(argv[0]);
		return -1;
	}

	if (argc > 4 && string(argv[3]) == "-t")
	{
		TH = atof(argv[4])/256.0;
	}
	const string img_dir = argv[1];	
	const string result_fn = argv[2];

	if (_access(img_dir.c_str(), 0) < 0 )
	{
		std::cout << "Input path not exist!" << std::endl;
		return PATH_NOT_EXIST;
	}
	vector<string> files;	
	if (getFiles(img_dir, "*.tif", files) < 0)
	{
		std::cout << "get files in directory " << img_dir << " error!" << std::endl;
		return GET_IMAGES_FAILED;
	}
	
	vector<vector<Inpurity>> inps_vec;
	vector<string> processed_files;
	for each (auto file in files)
	{
		Mat img = cv::imread(file, CV_LOAD_IMAGE_ANYDEPTH);
		if (img.empty())
		{
			continue;
		}
		Mat img_8bit;
		img.convertTo(img_8bit, CV_8U, 1 / 256.0f);
		Mat binary_img;
		double otsu_th = threshold(img_8bit, binary_img, 0.5f, 1, cv::THRESH_BINARY | cv::THRESH_OTSU);	
		Mat tmp_m, tmp_sd;
		float th = TH;
		if (otsu_th < TH)
		{
			th = getImgTh(img, otsu_th);			
		}
		else
		{
			Mat mask = 1 - binary_img;
			meanStdDev(img_8bit, tmp_m, tmp_sd, mask);
			double m = tmp_m.at<double>(0, 0);
			double sd = tmp_sd.at<double>(0, 0);
			th = m + 3 * sd;
		}
		threshold(img_8bit, binary_img, th, 1, cv::THRESH_BINARY);		
		//imwrite("./bin_img.tif", binary_img);
		morphologyProcess(binary_img);		
		//imwrite("./mor_img.tif", binary_img);
		Mat lable_img;
		std::map<int, int> cc;
		icvprCcaByTwoPass(binary_img, lable_img, cc);
		//imwrite("./two_passing_img.tif", lable_img);
		const int img_size = img.cols * img.rows;
		vector<Inpurity> inps = { Inpurity(),Inpurity(),Inpurity() };
		if (statInpuritySize(cc, inps) < 0)
			continue;
		for(auto it = inps.begin(); it != inps.end() ; ++it)
		{
			it->area_pro = static_cast<double>(it->sum_area)*100.0 / static_cast<double>(img_size);
		}
		inps_vec.push_back(inps);
		processed_files.push_back(file);
		//cv::Mat mask = binary_img < 1;
		//meanStdDev(, OutputArray mean, OutputArray stddev, InputArray mask = noArray());
	}
	writeResult(result_fn, processed_files, inps_vec);
	
	return 0;
}